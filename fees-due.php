<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>SMS | DUES</title>
    <?php
        include('header_files.php');
		@$status = $_GET['status'];
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2> SENT SMS TO UNPAID</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> SMS Sent Successfully.
                            </div>';
						}elseif(@$status == 'errors_id')
						{
							echo'<div class="alert alert-danger">
                                <strong>Error</strong> Please Refresh page and fill it again.
                            </div>';
						}elseif(@$status == 'error')
						{
							echo'<div class="alert alert-danger">
                                <strong>SMS OFF</strong> Please On SMS
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                               SENT SMS TO UNPAID
                            </h2>
                            
                        </div>  
						    
						<div class="body">
                            <form method="POST"  action="fees-due-exec.php">
                                
								<label for="email_address">ENTER MESSAGE</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea  id="msg" name="msg" required class="form-control" placeholder="Enter Message"></textarea>
                                    </div>
                                </div>
								
								<label for="year">SELECT CRITERIA</label>
								<div class="form-group">
                                            <select class="form-control show-tick" required name="criteria" id="criteria" onchange="getSmsCr(this.value)">
                                             <option value="">SELECT CRITERIA</option>
											 <option value="course">COURSE WISE</option>  
											 <option value="branch">BRANCH WISE</option> 
											 <option value="all">ALL</option> 
                                            </select>
                                </div>
								
								
								
								<div id="txt2"></div>

								<label for="email_address">ENTER FEES AMT(Greater Than)</label>
								<div class="form-group">
                                    <div class="form-line">
                                        <input type="text"  id="amt" name="amt" required class="form-control" placeholder="Enter Amt">
                                    </div>
                                </div>
								<button type="SUBMIT" class="btn btn-primary m-t-15 waves-effect">SENT</button>
								
                            </form>

                        </div>
						
                    </div>
                </div>
            </div>
        </div>
		
    </section>

    
  <script language="JavaScript">
  function demo(str)
{
	var a = confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='delete-fees.php?fc_id='+str; 
	}
}

function getSmsCr(str) {
	//var y_id = document.getElementById('y_id').value;
	if (str == "") {
		
		document.getElementById("txt2").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt2").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getSmsCr.php?value="+str,true);
        xmlhttp.send();
	
		
    }
    }
	
	function getFees(str) {
		var s_id = document.getElementById('s_id').value;
		var y_id = document.getElementById('y_id').value;
		//alert(y_id);
	if (str == "") {
        document.getElementById("txt3").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt3").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getFees.php?value="+s_id+"&y_id="+y_id,true);
        xmlhttp.send();
	
		
    }
    }
	

  </script>
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
