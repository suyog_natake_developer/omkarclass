<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Edit-course</title>
    <?php
        include('header_files.php');
		require_once("dbhost.php"); 
		@$status = $_GET['status'];
		$cr_id = @$_GET['cr_id'];
		$query = mysqli_query($con,"SELECT * FROM course WHERE `cr_id`='$cr_id'");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {
											$cr_name = $row['cr_name'];
											$cr_details = $row['cr_details'];
											$cr_fees = $row['cr_fees'];
											$cr_duration = $row['cr_duration'];
										}
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2>EDIT COURSE</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully addedd Course.
                            </div>';
						}elseif(@$status == 'editsuccess')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Edited Record.
                            </div>';
						}elseif(@$status == 'deletesuccess')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deleted</strong> You successfully deleted Record.
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                                EDIT COURSE
                            </h2>
                            
                        </div>  
						    
						<div class="body">
                            <form method="POST" action="edit-course-exec.php">
                                <label for="email_address">COURSE NAME</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" required id="cr_name" value="<?php echo $cr_name;?>" name="cr_name" class="form-control" placeholder="Enter Course Name">
                                    </div>
                                </div>
								<input type="hidden" name="cr_id" value="<?php echo $cr_id;?>">
                                <label for="details">COURSE DETAILS</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="cr_details" name="cr_details" value="<?php echo $cr_details;?>" class="form-control" placeholder="Enter Course Details">
                                    </div>
                                </div>
								<label for="details">COURSE FEES</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="cr_fees" required name="cr_fees" value="<?php echo $cr_fees;?>"  class="form-control" placeholder="Enter Fees">
                                    </div>
                                </div>
							    <label for="details">COURSE DURATION</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="cr_duration" name="cr_duration" value="<?php echo $cr_duration;?>" class="form-control" placeholder="Enter Duration">
                                    </div>
                                </div>							
							  <button type="SUBMIT" class="btn btn-primary m-t-15 waves-effect">SAVE</button>
                            </form>

                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </section>

  
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
