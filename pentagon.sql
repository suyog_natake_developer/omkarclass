-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 24, 2019 at 10:38 AM
-- Server version: 5.1.53
-- PHP Version: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pentagon`
--

-- --------------------------------------------------------

--
-- Table structure for table `admissions`
--

CREATE TABLE IF NOT EXISTS `admissions` (
  `ad_id` int(100) NOT NULL AUTO_INCREMENT,
  `s_id` int(100) DEFAULT NULL,
  `br_id` int(100) DEFAULT NULL,
  `y_id` int(100) DEFAULT NULL,
  `cr_id` varchar(100) DEFAULT NULL,
  `fees` varchar(100) DEFAULT NULL,
  `active_status` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ad_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `admissions`
--

INSERT INTO `admissions` (`ad_id`, `s_id`, `br_id`, `y_id`, `cr_id`, `fees`, `active_status`, `date`) VALUES
(1, 1, 1, 1, '1', '11000', 'active', '2018-09-26'),
(2, 2, 1, 1, '2', '7500', 'active', '2018-09-26'),
(3, 3, 1, 1, '2', '7500', 'active', '2018-09-26'),
(4, 4, 1, 1, '2', '7500', 'active', '2018-09-26'),
(5, 5, 1, 1, '1', '11000', 'active', '2018-09-26'),
(6, 6, 1, 1, '2', '7500', 'active', '2018-09-26'),
(7, 7, 1, 1, '2', '7500', 'active', '2018-09-26'),
(8, 8, 1, 1, '2', '7500', 'active', '2018-09-26'),
(9, 9, 1, 1, '2', '7500', 'active', '2018-09-26'),
(10, 10, 1, 1, '2', '7500', 'active', '2018-09-26'),
(26, 26, 2, 1, '2', '7500', 'active', '2018-10-02'),
(12, 12, 2, 1, '6', '9000', 'active', '2018-09-27'),
(13, 13, 2, 1, '6', '9000', 'active', '2018-09-27'),
(14, 14, 2, 1, '6', '9000', 'active', '2018-09-29'),
(15, 15, 2, 1, '6', '9000', 'active', '2018-09-29'),
(16, 16, 2, 1, '6', '9000', 'active', '2018-09-29'),
(17, 17, 2, 1, '6', '9000', 'active', '2018-09-29'),
(18, 18, 2, 1, '6', '9000', 'active', '2018-09-29'),
(19, 19, 2, 1, '6', '9000', 'active', '2018-09-29'),
(20, 20, 2, 1, '6', '9000', 'active', '2018-09-29'),
(21, 21, 2, 1, '6', '9000', 'active', '2018-09-29'),
(27, 27, 1, 1, '2', '7500', 'active', '2018-10-02'),
(23, 23, 2, 1, '2', '7500', 'active', '2018-10-02'),
(24, 24, 2, 1, '2', '7500', 'active', '2018-10-02'),
(25, 25, 1, 1, '2', '7500', 'active', '2018-10-02'),
(28, 28, 1, 1, '2', '7500', 'active', '2018-10-02'),
(29, 29, 1, 1, '2', '7500', 'active', '2018-10-02'),
(30, 30, 1, 1, '5', '4000', 'active', '2018-10-02'),
(31, 31, 1, 1, '5', '4000', 'active', '2018-10-02'),
(32, 32, 1, 1, '5', '4000', 'active', '2018-10-02'),
(33, 33, 1, 1, '4', '6000', 'active', '2018-10-02'),
(34, 34, 1, 1, '4', '6000', 'active', '2018-10-02'),
(35, 35, 1, 1, '4', '6000', 'active', '2018-10-02'),
(36, 36, 1, 1, '2', '7500', 'active', '2018-10-02'),
(37, 37, 1, 1, '3', '6500', 'active', '2018-10-02'),
(38, 38, 1, 1, '3', '6500', 'active', '2018-10-02'),
(39, 39, 1, 1, '3', '6500', 'active', '2018-10-02');

-- --------------------------------------------------------

--
-- Table structure for table `atten`
--

CREATE TABLE IF NOT EXISTS `atten` (
  `at_id` int(100) NOT NULL AUTO_INCREMENT,
  `s_id` int(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  PRIMARY KEY (`at_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `atten`
--


-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `br_id` int(100) NOT NULL AUTO_INCREMENT,
  `br_name` varchar(100) DEFAULT NULL,
  `br_details` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`br_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`br_id`, `br_name`, `br_details`) VALUES
(1, 'BAZAR TAL', 'Bazar tal '),
(2, 'KANDA MARKET', 'Kanda market road');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `cr_id` int(100) NOT NULL AUTO_INCREMENT,
  `cr_name` varchar(100) DEFAULT NULL,
  `cr_details` varchar(100) DEFAULT NULL,
  `cr_fees` varchar(100) DEFAULT NULL,
  `cr_duration` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`cr_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`cr_id`, `cr_name`, `cr_details`, `cr_fees`, `cr_duration`) VALUES
(1, 'Std 10 all subjects', '', '11000', 'Annual'),
(2, 'Std 10 maths and science', 'Maths and science', '7500', 'Annual'),
(3, 'Std 9', 'Maths and science', '6500', 'Annual'),
(4, 'Mix batch all subjects', '', '6000', 'Annual'),
(5, 'Mix batch M S', 'Maths and science', '4000', 'Annual'),
(6, 'Std 12', 'Maths', '9000', 'Annual');

-- --------------------------------------------------------

--
-- Table structure for table `fees_collect`
--

CREATE TABLE IF NOT EXISTS `fees_collect` (
  `fc_id` int(100) NOT NULL AUTO_INCREMENT,
  `s_id` varchar(100) DEFAULT NULL,
  `amt` varchar(100) DEFAULT NULL,
  `narration` varchar(100) DEFAULT NULL,
  `y_id` int(100) DEFAULT NULL,
  `cr_id` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  PRIMARY KEY (`fc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `fees_collect`
--


-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE IF NOT EXISTS `sms` (
  `sms_id` int(100) NOT NULL AUTO_INCREMENT,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`sms_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`sms_id`, `status`) VALUES
(1, 'off');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `s_id` int(100) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(100) DEFAULT NULL,
  `s_address` varchar(100) DEFAULT NULL,
  `s_school` varchar(100) DEFAULT NULL,
  `s_mob` varchar(100) DEFAULT NULL,
  `p_mob` varchar(100) DEFAULT NULL,
  `s_class` varchar(100) DEFAULT NULL,
  `y_id` int(100) DEFAULT NULL,
  `cr_id` varchar(100) DEFAULT NULL,
  `br_id` int(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`s_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`s_id`, `s_name`, `s_address`, `s_school`, `s_mob`, `p_mob`, `s_class`, `y_id`, `cr_id`, `br_id`, `date`) VALUES
(1, 'Pratik hirulkar', 'Shrirampur', 'De paul ', '', '9850853843', '10', 1, '1', 1, '2018-09-26'),
(2, 'Swapnil adhav', 'Shrirampur', 'St xeviers school', '', '8149758778', '10', 1, '2', 1, '2018-09-26'),
(3, 'Kaushal shelat', 'Shrirampur', 'Xeviers school', '', '9226202330', '10', 1, '2', 1, '2018-09-26'),
(4, 'Samarth abak', 'Shrirampur', 'Xeviers school', '', '9422792155', '10', 1, '2', 1, '2018-09-26'),
(5, 'Krishna yadav', 'Shrirampur', 'Nes', '', '9890052817', '10', 1, '1', 1, '2018-09-26'),
(6, 'Abhiraj amolik', 'Shrirampur', 'De paul', '', '9284634791', '10', 1, '2', 1, '2018-09-26'),
(7, 'Pawan patil', 'Shrirampur', 'De paul', '', '9850351627', '10', 1, '2', 1, '2018-09-26'),
(8, 'Durgesh shirsath', 'Tilaknagar', 'De paul', '', '8308085469', '10', 1, '2', 1, '2018-09-26'),
(9, 'Samyak zanjari', 'Shrirampur', 'De paul', '', '9975725077', '10', 1, '2', 1, '2018-09-26'),
(10, 'Vaishnavi shinde', 'Dattanagar', 'De paul', '', '7276244883', '10', 1, '2', 1, '2018-09-26'),
(11, 'Payal Jadhav', 'Shrirampur', 'De paul', '', '9112406848', '10', 1, '2', 1, '2018-09-26'),
(12, 'Santosh yadav', 'Shrirampur', 'Nes', '', '+919890450724', '12', 1, '6', 2, '2018-09-27'),
(13, 'Rajan yadav', 'Shrirampur', 'Nes', '', '+919960913783', '12', 1, '6', 2, '2018-09-27'),
(14, 'Rajan yadav', 'Shrirampur', 'Nes', '', '9975343783', '12', 1, '6', 2, '2018-09-29'),
(15, 'Raj singh', 'Shrirampur', 'Rbnb', '', '8208067785', '12', 1, '6', 2, '2018-09-29'),
(16, 'Santosh Yadav', 'Shrirampur', 'Nes', '9890450724', '9890450724', '12', 1, '6', 2, '2018-09-29'),
(17, 'Saurabh Bhosale', 'Shrirampur', 'Nes', '8788875570', '878887557', '12', 1, '6', 2, '2018-09-29'),
(18, 'Vaibhav sulakhe', 'Shrirampur', 'Nes', '', '8637702705', '12', 1, '6', 2, '2018-09-29'),
(19, 'Rinkesh zanjari', 'Shrirampur', 'Dahanukars', '+917040548575', '+917040548575', '12', 1, '6', 2, '2018-09-29'),
(20, 'Aniket gadekar', 'Shrirampur', 'Dahanukar', '+917218186378', '+917218186378', '12', 1, '6', 2, '2018-09-29'),
(21, 'Hrithik gorane', 'Shrirampur', 'Trimurti', '8796515230', '8796515230', '12', 1, '6', 2, '2018-09-29'),
(22, 'Payal Jadhav', 'Shrirampur', 'D-Paul', '9112406848', '9112406848', '10', 1, '2', 1, '2018-10-02'),
(23, 'Kasturi Bhagat', 'Shrirampur', 'D-Paul', '9422222513', '9422222513', '10', 1, '2', 2, '2018-10-02'),
(24, 'Srushti hiray', 'Shrirampur', 'D-Paul', '9850136368', '9011648193', '10', 1, '2', 2, '2018-10-02'),
(25, 'Vaishnavi morge', 'Shrirampur', 'D-Paul', '9822401410', '9822401410', '10', 1, '2', 1, '2018-10-02'),
(26, 'Jaya gaud', 'Shrirampur', 'D-Paul', '9175035737', '9175035737', '10', 1, '2', 2, '2018-10-02'),
(27, 'Chaitali morge', 'Shrirampur', 'D-Paul', '9028646896', '9028646896', '10', 1, '2', 1, '2018-10-02'),
(28, 'Shriya kshirsagar', 'Shrirampur', 'D-Paul', '8793645056', '9270575050', '10', 1, '2', 1, '2018-10-02'),
(29, 'Krishna trivedi', 'Shrirampur', 'D-Paul', '9762758455', '9762758455', '10', 1, '2', 1, '2018-10-02'),
(30, 'Samartha morge', 'Shrirampur', 'D-Paul', '7350305052', '7350305052', '8', 1, '5', 1, '2018-10-02'),
(31, 'Gaurav patil', 'Shrirampur', 'D-Paul', '7588054603', '7588054603', '8', 1, '5', 1, '2018-10-02'),
(32, 'Purva kalan', 'Shrirampur', 'D-Paul', '9421609414', '9421609414', '8', 1, '5', 1, '2018-10-02'),
(33, 'Om Rane', 'Shrirampur', 'D-Paul', '9766368404', '9766368404', '8', 1, '4', 1, '2018-10-02'),
(34, 'Preet chug', 'Shrirampur', 'D-Paul', '7304050504', '7304050504', '6', 1, '4', 1, '2018-10-02'),
(35, 'Prem chug', 'Shrirampur', 'Xeviour', '9273723160', '9273723160', '6', 1, '4', 1, '2018-10-02'),
(36, 'Yash Dudhediya', 'Shrirampur', 'D-Paul', '9145638955', '9145638955', '8', 1, '2', 1, '2018-10-02'),
(37, 'Girish gore', 'Shrirampur', 'Dahanukar', '9284428408', '9284428408', '9', 1, '3', 1, '2018-10-02'),
(38, 'Mukund Dawkhar', 'Shrirampur', 'Xeviour', '9850183263', '9850183263', '9', 1, '3', 1, '2018-10-02'),
(39, 'Atharva Desale', 'Shrirampur', 'NES', '9834645219', '9834645219', '9', 1, '3', 1, '2018-10-02');

-- --------------------------------------------------------

--
-- Table structure for table `student_cr`
--

CREATE TABLE IF NOT EXISTS `student_cr` (
  `crd_id` int(100) NOT NULL AUTO_INCREMENT,
  `fc_id` int(100) DEFAULT NULL,
  `s_id` varchar(100) DEFAULT NULL,
  `amt` varchar(100) DEFAULT NULL,
  `narration` varchar(100) DEFAULT NULL,
  `y_id` int(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`crd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `student_cr`
--


-- --------------------------------------------------------

--
-- Table structure for table `student_dr`
--

CREATE TABLE IF NOT EXISTS `student_dr` (
  `d_id` int(100) NOT NULL AUTO_INCREMENT,
  `s_id` int(100) DEFAULT NULL,
  `amt` varchar(100) DEFAULT NULL,
  `narration` varchar(100) DEFAULT NULL,
  `y_id` int(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`d_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `student_dr`
--

INSERT INTO `student_dr` (`d_id`, `s_id`, `amt`, `narration`, `y_id`, `date`) VALUES
(1, 1, '11000', 'Class Joined(Std 10 all subjects)', 1, '2018-09-26'),
(2, 2, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-09-26'),
(3, 3, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-09-26'),
(4, 4, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-09-26'),
(5, 5, '11000', 'Class Joined(Std 10 all subjects)', 1, '2018-09-26'),
(6, 6, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-09-26'),
(7, 7, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-09-26'),
(8, 8, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-09-26'),
(9, 9, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-09-26'),
(10, 10, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-09-26'),
(11, 11, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-09-26'),
(12, 12, '9000', 'Class Joined(Std 12)', 1, '2018-09-27'),
(13, 13, '9000', 'Class Joined(Std 12)', 1, '2018-09-27'),
(14, 14, '9000', 'Class Joined(Std 12)', 1, '2018-09-29'),
(15, 15, '9000', 'Class Joined(Std 12)', 1, '2018-09-29'),
(16, 16, '9000', 'Class Joined(Std 12)', 1, '2018-09-29'),
(17, 17, '9000', 'Class Joined(Std 12)', 1, '2018-09-29'),
(18, 18, '9000', 'Class Joined(Std 12)', 1, '2018-09-29'),
(19, 19, '9000', 'Class Joined(Std 12)', 1, '2018-09-29'),
(20, 20, '9000', 'Class Joined(Std 12)', 1, '2018-09-29'),
(21, 21, '9000', 'Class Joined(Std 12)', 1, '2018-09-29'),
(22, 22, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-10-02'),
(23, 23, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-10-02'),
(24, 24, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-10-02'),
(25, 25, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-10-02'),
(26, 26, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-10-02'),
(27, 27, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-10-02'),
(28, 28, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-10-02'),
(29, 29, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-10-02'),
(30, 30, '4000', 'Class Joined(Mix batch M S)', 1, '2018-10-02'),
(31, 31, '4000', 'Class Joined(Mix batch M S)', 1, '2018-10-02'),
(32, 32, '4000', 'Class Joined(Mix batch M S)', 1, '2018-10-02'),
(33, 33, '6000', 'Class Joined(Mix batch all subjects)', 1, '2018-10-02'),
(34, 34, '6000', 'Class Joined(Mix batch all subjects)', 1, '2018-10-02'),
(35, 35, '6000', 'Class Joined(Mix batch all subjects)', 1, '2018-10-02'),
(36, 36, '7500', 'Class Joined(Std 10 maths and science)', 1, '2018-10-02'),
(37, 37, '6500', 'Class Joined(Std 9)', 1, '2018-10-02'),
(38, 38, '6500', 'Class Joined(Std 9)', 1, '2018-10-02'),
(39, 39, '6500', 'Class Joined(Std 9)', 1, '2018-10-02');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `gender` varchar(50) NOT NULL,
  `role` varchar(50) DEFAULT NULL,
  `l_id` int(100) DEFAULT NULL,
  `c_id` int(100) DEFAULT NULL,
  `userid` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `address`, `mobile`, `email`, `gender`, `role`, `l_id`, `c_id`, `userid`, `password`, `date`, `time`) VALUES
(1, 'Admin', 'Mahegaon', '9273991626', 'kiranubale74@gmail.com', 'male', 'Admin', NULL, NULL, 'Admin', '12345', '2016-10-07', '12:53:11');

-- --------------------------------------------------------

--
-- Table structure for table `year`
--

CREATE TABLE IF NOT EXISTS `year` (
  `y_id` int(100) NOT NULL AUTO_INCREMENT,
  `year` varchar(100) DEFAULT NULL,
  `active_status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`y_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `year`
--

INSERT INTO `year` (`y_id`, `year`, `active_status`) VALUES
(1, '2018-19', 'active'),
(2, '2019-20', 'deactive');
