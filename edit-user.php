<?php

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Edit-Branch</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

    <!-- Wait Me Css -->
    <link href="plugins/waitme/waitMe.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />

     <!-- JQuery DataTable Css -->
    <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <?php
    include('header.php');
    include('menu.php');
    require_once("config.php"); 
    $userid = $_GET['userid'];
    ?>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>ADD USER FORM</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <?php
                                $query = mysqli_query($con,"SELECT * FROM user WHERE `user_id` = '$userid'");
                                while ($row = mysqli_fetch_assoc($query))
                                {
                                 $username = $row['username'];
                                 $address = $row['address'];
                                 $mobile = $row['mobile'];
                                 $email = $row['email'];
                                 $gender = $row['gender'];
                                 $role = $row['role'];
                                 $userid = $row['userid'];
                                 echo $role;
                                }
                            ?>
                        <h2 class="card-inside-title">  User</h2>
                            <form class="form-horizontal" method="POST" action="edit-user-exec.php">
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="name">Name</label>
                                    </div>
                                    <div class="col-lg-5 col-lg-offset-0">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="name" name="u_name" class="form-control" value="<?php echo $username;?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="address">Address</label>
                                    </div>
                                    <div class="col-lg-5 col-lg-offset-0">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="address" name="u_address" class="form-control" value="<?php echo $address;?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="mobile">Mobile</label>
                                    </div>
                                    <div class="col-lg-5 col-lg-offset-0">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" id="mobile" name="u_mobile" maxlength="10" class="form-control" value="<?php echo $mobile;?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email">Email</label>
                                    </div>
                                    <div class="col-lg-5 col-lg-offset-0">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="mail" id="mail" name="u_mail" class="form-control" value="<?php echo $email;?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                    if($gender == "male")
                                    {
                                      echo'<div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="gender">Gender</label>
                                    </div>
                                    <div class="col-lg-5 col-lg-offset-0">
                                        <div class="form-group">
                                            <input name="u_gender" type="radio" class="with-gap" id="radio_3" value="male" required/>
                                            <label for="radio_3">Male</label>
                                            <div class="col-lg-5 col-lg-offset-0">
                                        
                                        <div class="form-group">
                                            <input name="u_gender" type="radio" id="radio_4" class="with-gap" value="female" required/>
                                            <label for="radio_4">Female</label>
                                        </div>
                                    </div>';  
                                    }

                                    else if($gender == "female")
                                    {
                                        echo '<div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="gender">Gender</label>
                                    </div>
                                    <div class="col-lg-5 col-lg-offset-0">
                                        <div class="form-group">
                                            <input name="u_gender" type="radio" class="with-gap" id="radio_3" value="male" required/>
                                            <label for="radio_3">Male</label>
                                            <div class="col-lg-5 col-lg-offset-0">
                                        
                                        <div class="form-group">
                                            <input name="u_gender" type="radio" id="radio_4" class="with-gap" value="female" required/>
                                            <label for="radio_4">Female</label>
                                        </div>
                                    </div>';
                                    }
                                ?>
                               <!--  <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="gender">Gender</label>
                                    </div>
                                    <div class="col-lg-5 col-lg-offset-0">
                                        <div class="form-group">
                                            <input name="gender" type="radio" class="with-gap" id="radio_3" value="male" required/>
                                            <label for="radio_3">Male</label>
                                            <input name="gender" type="radio" id="radio_4" class="with-gap" value="female" required/>
                                            <label for="radio_4">Female</label>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="role">Role</label>
                                    </div>
                                    <div class="col-lg-5 col-lg-offset-0">
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="u_role" value="<?php echo $role;?>">
                                                <option value="">-- Please select --</option>
                                                <option value="admin">Admin</option>
                                                <option value="carret">Sales Executive</option>
                                                <option value="account">Account</option>
                                                <option value="bill">Dealer</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="uername">User Name</label>
                                    </div>
                                    <div class="col-lg-5 col-lg-offset-0">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="u_username" id="username" class="form-control" value="value="<?php echo $userid;?>"">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password">Password</label>
                                    </div>
                                    <div class="col-lg-5 col-lg-offset-0">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="password" name="u_password" id="password" class="form-control" value="<?php echo $password;?>"">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-lg-offset-2">
                                         <button type="SUBMIT" class="btn btn-primary waves-effect">SAVE</button>
                                    </div>
                                    <div class="col-lg-2 col-lg-offset-0">
                                         <button type="RESET" class="btn bg-black waves-effect waves-light">CLEAR</button>
                                    </div>
                                </div>
                            </form>                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content" style="margin-top:0px;">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ALL USERS
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                            <th>Username</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                    <?php
                                        require_once("dbhost.php"); 

                                        $query = mysqli_query($con,"SELECT * FROM user");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {
                                            echo"<tr>"; 
                                            echo '<td>'.$row['username'].'</td>';
                                            echo '<td>'.$row['mobile'].'</td>';
                                            echo '<td>'.$row['email'].'</td>';
                                            echo '<td>'.$row['userid'].'</td>';
                                            echo'<td><a href="edit-user.php?userid='.$row['user_id'].'"><i class="material-icons">mode_edit</i>&ensp;&ensp;&ensp;</a>

                                            <a href="javascript:delete_id('.$row['user_id'].')"><i class="material-icons">delete</i>&ensp;&ensp;&ensp;</a>

                                            <a href="change-pass.php?userid='.$row['user_id'].'" <i class="material-icons">remove_red_eye</i></a></td>';
                                        }
                                    ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/forms/basic-form-elements.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script> -->


    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
</body>
</html>
