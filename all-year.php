<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Academic Year</title>
    <?php
        include('header_files.php');
		@$status = $_GET['status'];
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
    ?>
   

    <section class="content" style="margin-top:100px;">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ACTIVE/DEACTIVE ACADEMIC YEAR
                            </h2>
                        </div>
						<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Success</strong> 
                            </div>';
						}elseif(@$status == 'editsuccess')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Edited Record.
                            </div>';
						}elseif(@$status == 'exist')
						{
							echo'<div class="alert alert-danger">
                                <strong>Exist</strong>Already One year is activated.
                            </div>';
						}
						?>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Year</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        require_once("dbhost.php"); 

                                        $query = mysqli_query($con,"SELECT * FROM year");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {
                                       echo'<tr>';
                                       echo'<td>'.$row['year'].'</td>';
									   $active_status = $row['active_status'];
								       if($active_status == 'active')
									   {
                                       echo'<td><a href="deactive-year.php?y_id='.$row['y_id'].'" ><button type="button" class="btn btn-primary waves-effect">
											<i class="material-icons">clear</i>
											<span>DEACTIVE</span>
											</button></a>';
									   }else
									   {
										   echo'<td><a href="active-year.php?y_id='.$row['y_id'].'" ><button type="button" class="btn btn-primary waves-effect">
											<i class="material-icons">font_download</i>
											<span>ACTIVE</span>
											</button></a>';
									   }
											
										
                                       echo'</tr>';
                                        }
                                        ?>                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
  <script language="JavaScript">
  function demo(str)
{
	var a = confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='delete-branch.php?br_id='+str; 
	}
}
  </script>
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
