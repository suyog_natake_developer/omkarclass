<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Forward | Edit</title>
    <?php
        include('header_files.php');
		@$status = $_GET['status'];
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2> FORWARD STUDENTS</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> Operation Success.
                            </div>';
						}elseif(@$status == 'errors_id')
						{
							echo'<div class="alert alert-danger">
                                <strong>Error</strong> Please Refresh page and fill it again.
                            </div>';
						}elseif(@$status == 'error1')
						{
							echo'<div class="alert alert-danger">
                                <strong>Error</strong> Duplicate Record Fill it again
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                                FORWARD STUDENTS
                            </h2>
                            
                        </div>  
						    
						<div class="body">
                            <form method="POST"  action="forward-exec.php">
                                
							<?php 
										include('dbhost.php');
										$query12 = mysqli_query($con,"SELECT * FROM year WHERE `active_status`='active'");
                                        while ($row12 = mysqli_fetch_assoc($query12))
                                        {		
												$y_id = $row12['y_id'];
                                          
										}
										?>
										<input type="hidden"  name="y_id" id="y_id" value="<?php echo $y_id;?>">
										
								
								<label for="year">BRANCH</label>
								<div class="form-group">
                                            <select class="form-control show-tick" name="br_id" id="br_id" onchange="getStudent(this.value)">
                                                <option value="">-- Please select --</option>
											<?php 
										include('dbhost.php');
										$query = mysqli_query($con,"SELECT * FROM branch");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['br_id'].'">'.$row['br_name'].'</option>';
                                          
										}
										?>
                                            </select>
                                </div>
								
								
								
								
								<label for="year">COURSES</label>
                                   <div class="form-group">
                                    <select class="form-control show-tick" multiple name="courses[]">
                                        <?php 
										//include('dbhost.php');
										$query = mysqli_query($con,"SELECT * FROM course");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['cr_id'].'">'.$row['cr_name'].'</option>';
                                          
										}
										?>
                                    </select>
									</div>
                                <div id="txt2"></div>
								
								<button type="SUBMIT" class="btn btn-primary m-t-15 waves-effect">SAVE</button>
								
                            </form>

                        </div>
						
                    </div>
                </div>
            </div>
        </div>
		<div id="txt3"></div>
    </section>

    
  <script language="JavaScript">
  function demo(str)
{
	var a = confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='delete-fees.php?fc_id='+str; 
	}
}

function getStudent(str) {
	var y_id = document.getElementById('y_id').value;
	if (str == "") {
		
		document.getElementById("txt2").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt2").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getStudent.php?br_id="+str+"&y_id="+y_id+"&status=forward",true);
        xmlhttp.send();
	
		
    }
    }
	
	function getFees(str) {
		var s_id = document.getElementById('s_id').value;
		var y_id = document.getElementById('y_id').value;
		//alert(y_id);
	if (str == "") {
        document.getElementById("txt3").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt3").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getFees.php?value="+s_id+"&y_id="+y_id,true);
        xmlhttp.send();
	
		
    }
    }
	

  </script>
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
