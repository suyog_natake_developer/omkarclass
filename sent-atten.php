<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>SMS | ATTEN</title>
    <?php
        include('header_files.php');
		@$status = $_GET['status'];
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2> SENT ATTENDENCE SMS</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> SMS Sent Successfully.
                            </div>';
						}elseif(@$status == 'errors_id')
						{
							echo'<div class="alert alert-danger">
                                <strong>Error</strong> Please Refresh page and fill it again.
                            </div>';
						}elseif(@$status == 'error1')
						{
							echo'<div class="alert alert-danger">
                                <strong>Error</strong> Duplicate Record Fill it again
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                               SENT ATTENDENCE SMS
                            </h2>
                            
                        </div>  
						    
						<div class="body">
                            <form method="POST"  action="sent-atten-exec.php">
                                
								<label for="year">SELECT FROM DATE</label>
								 <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" required name="f_date" id="f_date" class="datepicker form-control" placeholder="Please choose a date...">
                                        </div>
                                </div>
								
								<label for="year">SELECT TO DATE</label>
                                <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" required name="t_date" id="t_date" class="datepicker form-control" placeholder="Please choose a date...">
                                        </div>
                                </div>
                               
								<button type="SUBMIT" class="btn btn-primary m-t-15 waves-effect">SENT</button>
								
                            </form>

                        </div>
						
                    </div>
                </div>
            </div>
        </div>
		
    </section>

    
  <script language="JavaScript">
  function demo(str)
{
	var a = confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='delete-fees.php?fc_id='+str; 
	}
}

function getSmsCr(str) {
	//var y_id = document.getElementById('y_id').value;
	if (str == "") {
		
		document.getElementById("txt2").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt2").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getSmsCr.php?value="+str,true);
        xmlhttp.send();
	
		
    }
    }
	
	function getFees(str) {
		var s_id = document.getElementById('s_id').value;
		var y_id = document.getElementById('y_id').value;
		//alert(y_id);
	if (str == "") {
        document.getElementById("txt3").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt3").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getFees.php?value="+s_id+"&y_id="+y_id,true);
        xmlhttp.send();
	
		
    }
    }
	

  </script>
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
