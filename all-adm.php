<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Admissions</title>
    <?php
        include('header_files.php');
		@$status = $_GET['status'];
    ?>
</head>
    <?php
        include('header.php');
		include('dbhost.php');
        include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2>Deactive Admissions</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> Success.
                            </div>';
						}elseif(@$status == 'editsuccess')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Edited Record.
                            </div>';
						}elseif(@$status == 'deletesuccess')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deleted</strong> You successfully deleted Record.
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                                DEACTIVE ADMISSIONS
                            </h2>
                            
                        </div>  
					
					
                        <div class="body table-responsive">
						 <h5 align="center">Search:<input type="text" id="search" onkeyup="myFunction()"></h5>
                            <table class="table table-bordered" id="example">
                                <thead>
                                    <tr>
                                        <th>STUDENT NAME</th>
                                        <th>COURSE</th>
										<th>STATUS</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                   <?PHP
	

	$qry1 = mysqli_query($con,"SELECT * FROM `admissions` WHERE `active_status`='active'");
	while($row1 = mysqli_fetch_assoc($qry1))
	{
      $s_id = $row1['s_id'];
	  $qryst = mysqli_query($con,"SELECT * FROM `student` WHERE `s_id`='$s_id'");
	  while($rowst = mysqli_fetch_assoc($qryst))
	  {
		  $ad_id = $row1['ad_id'];
		  $qryst12 = mysqli_query($con,"SELECT * FROM `admissions` WHERE `ad_id`='$ad_id'");
	  while($rowst12 = mysqli_fetch_assoc($qryst12))
	  {
		  $cr_id12 = $rowst12['cr_id'];
	  }
	
$qryst1 = mysqli_query($con,"SELECT * FROM `course` WHERE `cr_id`='$cr_id12'");
	  while($rowst1 = mysqli_fetch_assoc($qryst1))
	  {
		  $cr_name = $rowst1['cr_name'];
	  }
	  
		   			echo'<tr>';
							echo'<td>'.$rowst['s_name'].'</td>';
							echo'<td>'.$cr_name.'</td>';
							echo'<td><a href="deactive.php?ad_id='.$row1['ad_id'].'"><button type="button" class="btn btn-primary waves-effect">
											<i class="material-icons">edit</i>
											<span>DEACTIVE</span>
											</button></a>
											
											
											</td>';
                            echo'</tr>';
			
	  }
	  
	}
 
								   ?>									
                                </tbody>
                            </table>
                        </div>
               
            <!-- #END# Bordered Table -->
					
                    </div>
					<div id="txt2"></div>
                </div>
            </div>
        </div>
    </section>
     <div id="txt3"></div>
    
  <script language="JavaScript">
  function demo(str)
{
	var a = confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='delete-branch.php?br_id='+str; 
	}
}

function getStudentList(str) {
	var y_id = document.getElementById('y_id').value;
	var br_id = document.getElementById('br_id').value;
	var cr_id = document.getElementById('cr_id').value;
	var date = document.getElementById('date').value;
	if (str == "") {
        document.getElementById("txt2").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt2").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getStudentList.php?y_id="+y_id+"&cr_id="+cr_id+"&br_id="+br_id+"&date="+date,true);
        xmlhttp.send();
	
		
    }
    }
	
	function getLedger(str) {
		//var f_date = document.getElementById('f_date').value;
		var yr_id = document.getElementById('yr_id').value;
		var s_id = document.getElementById('s_id').value;
		
	if (str == "") {
        document.getElementById("txt3").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt3").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","getLedger.php?value="+str+"&yr_id="+yr_id+"&s_id="+s_id,true);
        xmlhttp.send();
	
		
    }
    }
	
	function myFunction() {
  // Declare variables 
  var input, filter, table, tr, td, i;
  input = document.getElementById("search");
  filter = input.value.toUpperCase();
  table = document.getElementById("example");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}

	function deactive(str) {
		
	if (str == "") {
        document.getElementById("txt2").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt2").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","savePresent.php?value="+str+"&date="+date+"&br_id="+br_id+"&cr_id="+cr_id+"&y_id="+y_id,true);
        xmlhttp.send();
	
		
    }
    }
	
		function absent(str) {
		
		var y_id = document.getElementById('y_id').value;
		var br_id = document.getElementById('br_id').value;
		var cr_id = document.getElementById('cr_id').value;
		var date = document.getElementById('date').value;
	if (str == "") {
        document.getElementById("txt2").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt2").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","saveabsent.php?value="+str+"&date="+date+"&br_id="+br_id+"&cr_id="+cr_id+"&y_id="+y_id,true);
        xmlhttp.send();
	
		
    }
    }
  </script>
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
