<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Add-Student</title>
    <?php
        include('header_files.php');
		@$status = $_GET['status'];
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2>ADD STUDENT</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully addedd Student.
                            </div>';
						}elseif(@$status == 'editsuccess')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Edited Record.
                            </div>';
						}elseif(@$status == 'deletesuccess')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deleted</strong> You successfully deleted Record.
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                                ADD STUDENT DEATAILS
                            </h2>
                            
                        </div>  
						    
						<div class="body">
                            <form method="POST" action="add-student-exec.php">
							
							<label for="year">BRANCH</label>
                                
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="br_id" required>
                                                <option value="">-- Please select --</option>
											<?php 
										include('dbhost.php');
										$query123 = mysqli_query($con,"SELECT * FROM branch");
                                        while ($row123 = mysqli_fetch_assoc($query123))
                                        {		
												echo'<option value="'.$row123['br_id'].'">'.$row123['br_name'].'</option>';
                                          
										}
										?>
                                            </select>
                                        </div>
                                <label for="email_address">STUDENT NAME</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="s_name" name="s_name" required class="form-control" placeholder="Enter Student Name">
                                    </div>
                                </div>
                                <label for="details">STUDENT ADDRESS</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="s_address" name="s_address" value="Shrirampur" class="form-control" placeholder="Enter Address">
                                    </div>
                                </div>
								
								<label for="year">SCHOOL</label>
                                
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="s_school" id="s_school" required>
                                                <option value="">-- select School--</option>
												<option value="NES">New English School</option>
												<option value="D-Paul">D-Paul School</option>
												<option value="Xeviour">St. Xeviour</option>
												<option value="Model">Model English School</option>
												<option value="VidyaNiketan">VidyaNiketan School</option>
												<option value="Dahanukar">Dahanukar English School</option>
												<option value="CDJ">CDJ College</option>
												<option value="RBNB">RBNB College</option>
                                            </select>
                                        </div>
								
								<label for="details">STUDENT'S MOBILE</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="s_mob" name="s_mob"  class="form-control" placeholder="Enter Mobile">
                                    </div>
                                </div>
								
								<label for="details">PARENT'S MOBILE</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="p_mob" required name="p_mob"  class="form-control" placeholder="Enter Parent's Mobile">
                                    </div>
                                </div>
								
								<label for="details">CLASS</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="s_class" name="s_class"  class="form-control" placeholder="Enter Students Class">
                                    </div>
                                </div>
								
								<?php 
										include('dbhost.php');
										$query12 = mysqli_query($con,"SELECT * FROM year WHERE `active_status`='active'");
                                        while ($row12 = mysqli_fetch_assoc($query12))
                                        {		
												$y_id = $row12['y_id'];
                                          
										}
										?>
										<input type="hidden"  name="ac_year" value="<?php echo $y_id;?>">
                                           										
										
								 
                                        <label for="year">COURSES</label>
                                   
                                    <select class="form-control show-tick" multiple name="courses[]" required>
                                        <?php 
										//include('dbhost.php');
										$query = mysqli_query($con,"SELECT * FROM course");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['cr_id'].'">'.$row['cr_name'].'</option>';
                                          
										}
										?>
                                    </select>
                                 </br></br>								 
							  <button type="SUBMIT" class="btn btn-primary m-t-15 waves-effect">SAVE</button>
                            </form>

                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content" style="margin-top:0px;">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ALL STUDENTS	
                            </h2>
                        </div>
						
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>MOBILE</th>
                                            <th>PARENT MOBILE</th>
											<th>ADDRESS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        require_once("dbhost.php"); 

                                        $queryk = mysqli_query($con,"SELECT * FROM student");
                                        while ($rowk = mysqli_fetch_assoc($queryk))
                                        {
                                       echo'<tr>';
                                       echo'<td>'.$rowk['s_name'].'</td>';
                                       echo'<td>'.$rowk['s_mob'].'</td>';
									   echo'<td>'.$rowk['p_mob'].'</td>';
									   echo'<td>'.$rowk['s_address'].'</td>';
                                       echo'<td><a href="edit-student.php?s_id='.$rowk['s_id'].'" ><button type="button" class="btn btn-primary waves-effect">
											<i class="material-icons">edit</i>
											<span>EDIT</span>
											</button></a>
											
											<a href="javascript:demo('.$rowk['s_id'].')" type="button"> <button class="btn btn-danger waves-effect">
											<i class="material-icons">delete</i>
											<span>DELETE</span>
											</button></a>
											</td>';
                                       echo'</tr>';
                                        }
                                        ?>                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
  <script language="JavaScript">
  function demo(str)
{
	var a = confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='delete-student.php?s_id='+str; 
	}
}
  </script>
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
