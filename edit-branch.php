<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Edit-Branch</title>
    <?php
        include('header_files.php');
		@$status = $_GET['status'];
		require_once("dbhost.php");
		$br_id = $_GET['br_id'];

                                        $query = mysqli_query($con,"SELECT * FROM branch WHERE `br_id`='$br_id'");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {
											$br_name = $row['br_name'];
											$br_details = $row['br_details'];
										}
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2>EDIT BRANCH</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Edited Record.
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                                EDIT BRANCH
                            </h2>
                            
                        </div>  
						    
						<div class="body">
                            <form method="POST" action="edit-branch-exec.php">
                                <label for="email_address">BRANCH NAME</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="br_name" required value="<?php echo $br_name;?>" name="br_name" class="form-control" placeholder="Enter Branch Name">
                                    </div>
                                </div>
								<input type="hidden" name="br_id" value="<?php echo $br_id;?>">
                                <label for="details">BRANCH DETAILS</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="br_details" value="<?php echo $br_details;?>" name="br_details"  class="form-control" placeholder="Enter Branch Details">
                                    </div>
                                </div>
							  <button type="SUBMIT" class="btn btn-primary m-t-15 waves-effect">SAVE</button>
                            </form>

                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </section>

 

    <?php
        include('footer_files.php')
    ?>
</body>
</html>
