<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Edit-Student</title>
    <?php
        include('header_files.php');
		include('dbhost.php');
		@$status = @$_GET['status'];
		@$s_id = $_GET['s_id'];
		$query = mysqli_query($con,"SELECT * FROM student WHERE `s_id`='$s_id'");
                                        while($row = mysqli_fetch_assoc($query))
                                        {
											$s_name = $row['s_name'];
											$s_address = $row['s_address'];
											$s_school = $row['s_school'];
											$s_mob = $row['s_mob'];
											$p_mob = $row['p_mob'];
											$s_class = $row['s_class'];
											$br_id = $row['br_id'];
											
										}
					$query1 = mysqli_query($con,"SELECT * FROM branch WHERE `br_id`='$br_id'");
                                        while ($row1 = mysqli_fetch_assoc($query1))
                                        {
										  $br_name = $row1['br_name'];
										}									
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2>EDIT STUDENT</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					
                      <div class="header">
                            <h2>
                                EDIT STUDENT DEATAILS
                            </h2>
                            
                        </div>  
						    
						<div class="body">
                            <form method="POST" action="edit-student-exec.php">
							<input type="hidden" value="<?php echo $s_id;?>" name="s_id">
							<label for="year">BRANCH</label>
                                
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="br_id">
                                                <option value="<?php echo $br_name;?>"><?php echo $br_name;?></option>
											<?php 
										include('dbhost.php');
										$query = mysqli_query($con,"SELECT * FROM branch");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['br_id'].'">'.$row['br_name'].'</option>';
                                          
										}
										?>
                                            </select>
                                        </div>
                                <label for="email_address">STUDENT NAME</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="s_name" value="<?php echo $s_name;?>" name="s_name" class="form-control" placeholder="Enter Student Name">
                                    </div>
                                </div>
                                <label for="details">STUDENT ADDRESS</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="s_address" name="s_address" value="<?php echo $s_address;?>" class="form-control" placeholder="Enter Address">
                                    </div>
                                </div>
								
								<label for="details">STUDENT SCHOOL</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="s_school" name="s_school" value="<?php echo $s_school;?>" class="form-control" placeholder="Enter School">
                                    </div>
                                </div>
								
								<label for="details">STUDENT'S MOBILE</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="s_mob" name="s_mob" value="<?php echo $s_mob;?>" class="form-control" placeholder="Enter Mobile">
                                    </div>
                                </div>
								
								<label for="details">PARENT'S MOBILE</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="p_mob" name="p_mob" value="<?php echo $p_mob;?>" class="form-control" placeholder="Enter Parent's Mobile">
                                    </div>
                                </div>
								
								<label for="details">CLASS</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="s_class" name="s_class" value="<?php echo $s_class;?>" class="form-control" placeholder="Enter Students Class">
                                    </div>
                                </div>
								
							
                                 </br></br>								 
							  <button type="SUBMIT" class="btn btn-primary m-t-15 waves-effect">SAVE</button>
                            </form>

                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
        include('footer_files.php')
    ?>
</body>
</html>
