﻿<?php
include('dbhost.php');
$qry = mysqli_query($con,"SELECT SUM(fees) as fees FROM `admissions`");
while($row = mysqli_fetch_assoc($qry))
{
	@$total_amt = $row['fees'];
}

$qry1 = mysqli_query($con,"SELECT SUM(amt) as amt FROM `fees_collect`");
while($row1 = mysqli_fetch_assoc($qry1))
{
	@$total_received = $row1['amt'];
}

$total_bal = $total_amt - $total_received;

$qqry = mysqli_query($con,"SELECT * FROM `year` WHERE `active_status`='active'");
while($qrow = mysqli_fetch_assoc($qqry))
{
	$y_id = $qrow['y_id'];
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Omkar | Dashboard</title>
    <?php include('header_files.php');
	?>
</head>

<body class="theme-red">
    <!-- Page Loader -->

    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->

     <?php
	    include('header.php');
	 ?>

    <!-- #Top Bar -->
     <?php
        include('menu.php');
     ?>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
						
                        <div class="content">
                            <div class="text">TOTAL AMOUNT</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $total_amt;?>" data-speed="15" data-fresh-interval="20"><?php echo $total_amt;?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">account_balance_wallet</i>
                        </div>
                        <div class="content">
                            <div class="text">TOTAL RECEIVED</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $total_received;?>" data-speed="1000" data-fresh-interval="20"><?php echo $total_received;?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">account_balance</i>
                        </div>
                        <div class="content">
                            <div class="text">TOTAL BAL</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $total_bal;?>" data-speed="1000" data-fresh-interval="20"><?php echo $total_bal;?></div>
                        </div>
                    </div>
                </div>
				
				
                
            </div>
            <!-- #END# Widgets -->
			
			 <!-- Bordered Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                BATCH WISE COLLECTION
                                                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>BATCH</th>
                                        <th>TOTAL AMT</th>
                                        <th>REC AMT</th>
                                        <th>BAL AMT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?PHP
								   $qryk = mysqli_query($con,"SELECT * FROM `course`");
								   while($rowk = mysqli_fetch_assoc($qryk))
								   {
									   $cr_name = $rowk['cr_name'];
									   $cr_id = $rowk['cr_id'];
									$qryy = mysqli_query($con,"SELECT SUM(fees) as fees FROM `admissions` WHERE `cr_id`='$cr_id' AND `y_id`='$y_id'");
									while($rowy = mysqli_fetch_assoc($qryy))
									{
									  @$total_amt1 = $rowy['fees'];
									}
										
							      $qry1y = mysqli_query($con,"SELECT SUM(amt) as amt FROM `fees_collect` WHERE `cr_id`='$cr_id' AND `y_id`='$y_id'");
									while($row1y = mysqli_fetch_assoc($qry1y))
									{
									@$total_received1 = $row1y['amt'];
									}

									$total_bal1 = $total_amt1 - $total_received1;
									  echo'<tr>';
                                      echo'<th>'.$cr_name.'</th>';
                                      echo'<th>'.$total_amt1.'</th>';
									  echo'<th>'.$total_received1.'</th>';
									  echo'<th>'.$total_bal1.'</th>';
                                      echo'</tr>';
								   }
								   ?>									
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Bordered Table -->
			
             <!-- Bordered Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                YEAR WISE COLLECTION
                                                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>YEAR</th>
                                        <th>TOTAL AMT</th>
                                        <th>REC AMT</th>
                                        <th>BAL AMT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?PHP
								   $qryk = mysqli_query($con,"SELECT * FROM `year`");
								   while($rowk = mysqli_fetch_assoc($qryk))
								   {
									   $year = $rowk['year'];
									   $y_id = $rowk['y_id'];
									$qryy = mysqli_query($con,"SELECT SUM(fees) as fees FROM `admissions` WHERE `y_id`='$y_id'");
									while($rowy = mysqli_fetch_assoc($qryy))
									{
									  @$total_amt1 = $rowy['fees'];
									}

							      $qry1y = mysqli_query($con,"SELECT SUM(amt) as amt FROM `fees_collect` WHERE `y_id`='$y_id'");
									while($row1y = mysqli_fetch_assoc($qry1y))
									{
									@$total_received1 = $row1y['amt'];
									}

									$total_bal1 = $total_amt1 - $total_received1;
									  echo'<tr>';
                                      echo'<th>'.$year.'</th>';
                                      echo'<th>'.$total_amt1.'</th>';
									  echo'<th>'.$total_received1.'</th>';
									  echo'<th>'.$total_bal1.'</th>';
                                      echo'</tr>';
								   }
								   ?>									
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Bordered Table -->
			
			<!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                STUDENTS FEES BALANCE
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                           
                                            <th>BAL.AMT</th>
                                            
                                        </tr>
                                    </thead>
                                   
								   <?php
								   $qqry = mysqli_query($con,"SELECT * FROM `year` WHERE `active_status`='active'");
									while($qrow = mysqli_fetch_assoc($qqry))
									{
										$y_id = $qrow['y_id'];
									}
								     $qryk = mysqli_query($con,"SELECT DISTINCT(s_id) FROM `admissions` WHERE `active_status`='active'");
									 while($rowk = mysqli_fetch_assoc($qryk))
									 {
										 $s_id = $rowk['s_id'];
									
										 $qrys = mysqli_query($con,"SELECT * FROM `student` WHERE `s_id`='$s_id'");
										 while($rows = mysqli_fetch_assoc($qrys))
										 {
											 $s_name = $rows['s_name'];
											 echo'<tr>';
											 echo'<th>'.$s_name,'</td>';
									$qryy1 = mysqli_query($con,"SELECT SUM(fees) as fees FROM `admissions` WHERE `s_id`='$s_id'");
									while($rowy1 = mysqli_fetch_assoc($qryy1))
									{
									  @$total_amt11 = $rowy1['fees'];
									}

							      $qry1y1 = mysqli_query($con,"SELECT SUM(amt) as amt FROM `fees_collect` WHERE  `s_id`='$s_id'");
									while($row1y1 = mysqli_fetch_assoc($qry1y1))
									{
									@$total_received11 = $row1y1['amt'];
									}

									$total_bal11 = @$total_amt11 - @$total_received11;
									      echo'<th>'.$total_bal11.'</td>';
										  echo'</tr>';
										 }
									 }
								   ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <div class="row clearfix">
                <!-- Visitors -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-pink">
                            <div class="sparkline" data-type="line" data-spot-Radius="4" data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#fff"
                                 data-min-Spot-Color="rgb(255,255,255)" data-max-Spot-Color="rgb(255,255,255)" data-spot-Color="rgb(255,255,255)"
                                 data-offset="90" data-width="100%" data-height="92px" data-line-Width="2" data-line-Color="rgba(255,255,255,0.7)"
                                 data-fill-Color="rgba(0, 188, 212, 0)">
                                STUDENT COUNTS
                            </div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    TOTAL STUDENTS
									<?php
									$qryscnt = mysqli_query($con,"SELECT * FROM `admissions` WHERE `y_id`='$y_id'");
									$stdcnt = mysqli_num_rows($qryscnt)
									?>
                                    <span class="pull-right"><b><?php echo $stdcnt;?></b></span>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Visitors -->
				
				
				<!-- #END# Basic Examples -->
            <div class="row clearfix">
                <!-- Visitors -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-pink">
                            <div class="sparkline" data-type="line" data-spot-Radius="4" data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#fff"
                                 data-min-Spot-Color="rgb(255,255,255)" data-max-Spot-Color="rgb(255,255,255)" data-spot-Color="rgb(255,255,255)"
                                 data-offset="90" data-width="100%" data-height="92px" data-line-Width="2" data-line-Color="rgba(255,255,255,0.7)"
                                 data-fill-Color="rgba(0, 188, 212, 0)">
                                SMS COUNTER
                            </div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    TOTAL SMS BAL
									<?php
									$smsbal = file_get_contents($process1);
									?>
                                    <span class="pull-right"><b><?php echo $smsbal;?></b></span>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Visitors -->
               
            </div>
        </div>
    </section>

  <?php 
  include('footer_files.php');
  ?>
</body>

</html>
 
   
   </script>
   
