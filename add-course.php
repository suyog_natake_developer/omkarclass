<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Add-course</title>
    <?php
        include('header_files.php');
		@$status = $_GET['status'];
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2>ADD COURSE</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully addedd Course.
                            </div>';
						}elseif(@$status == 'editsuccess')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Edited Record.
                            </div>';
						}elseif(@$status == 'deletesuccess')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deleted</strong> You successfully deleted Record.
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                                ADD COURSE
                            </h2>
                            
                        </div>  
						    
						<div class="body">
                            <form method="POST" action="add-course-exec.php">
                                <label for="email_address">COURSE NAME</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="cr_name" required name="cr_name" class="form-control" placeholder="Enter Course Name">
                                    </div>
                                </div>
                                <label for="details">COURSE DETAILS</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="cr_details" name="cr_details"  class="form-control" placeholder="Enter Course Details">
                                    </div>
                                </div>
								<label for="details">COURSE FEES</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="cr_fees" name="cr_fees" required class="form-control" placeholder="Enter Fees">
                                    </div>
                                </div>
							    <label for="details">COURSE DURATION</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="cr_duration" name="cr_duration"  class="form-control" placeholder="Enter Duration">
                                    </div>
                                </div>							
							  <button type="SUBMIT" class="btn btn-primary m-t-15 waves-effect">SAVE</button>
                            </form>

                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content" style="margin-top:0px;">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ALL COURSE
                            </h2>
                        </div>
						
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>FEES</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        require_once("dbhost.php"); 

                                        $query = mysqli_query($con,"SELECT * FROM course");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {
                                       echo'<tr>';
                                       echo'<td>'.$row['cr_name'].'</td>';
                                       echo'<td>'.$row['cr_fees'].'</td>';
                                       echo'<td><a href="edit-course.php?cr_id='.$row['cr_id'].'" ><button type="button" class="btn btn-primary waves-effect">
											<i class="material-icons">edit</i>
											<span>EDIT</span>
											</button></a>
											
											<a href="javascript:demo('.$row['cr_id'].')" type="button"> <button class="btn btn-danger waves-effect">
											<i class="material-icons">delete</i>
											<span>DELETE</span>
											</button></a>
											</td>';
                                       echo'</tr>';
                                        }
                                        ?>                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
  <script language="JavaScript">
  function demo(str)
{
	var a = confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='delete-course.php?cr_id='+str; 
	}
}
  </script>
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
