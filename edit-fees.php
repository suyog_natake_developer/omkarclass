<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Fees | Collect</title>
    <?php
        include('header_files.php');
		include('dbhost.php');
		@$status = $_GET['status'];
		//@$fc_id = $_GET['fc_id'];
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
		@$fc_id = $_GET['fc_id'];
		$qry = mysqli_query($con,"SELECT * FROM `fees_collect` WHERE `fc_id`='$fc_id'");
		while($row = mysqli_fetch_assoc($qry))
		{
			$amt = $row['amt'];
			$narration = $row['narration'];
		}
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2>COLLECT FEES</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> Fees Collection Success.
                            </div>';
						}elseif(@$status == 'editsuccess')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Edited Record.
                            </div>';
						}elseif(@$status == 'deletesuccess')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deleted</strong> You successfully deleted Record.
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                                COLLECT FEES
                            </h2>
                            
                        </div>  
						    
						<div class="body">
                            <form method="POST" action="edit-fees-exec.php">
                                
							<label for="email_address">FEES AMOUNT</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="amt" value="<?php echo $amt;?>" name="amt" class="form-control" placeholder="Enter Amt">
                                    </div>
                                </div>
								
								<input type="hidden" name="fc_id" value="<?php echo $fc_id;?>">
								
								<label for="email_address">NARRATION</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="narration" value="<?php echo $narration;?>"  name="narration" class="form-control" placeholder="Enter Narration">
                                    </div>
                                </div>
										
							  <button type="SUBMIT" class="btn btn-primary m-t-15 waves-effect">SAVE</button>	
                              
								
                            </form>

                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </section>

    
 
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
