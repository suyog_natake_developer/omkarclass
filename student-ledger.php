<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Ledger</title>
    <?php
        include('header_files.php');
		@$status = $_GET['status'];
    ?>
</head>
    <?php
       include('header.php');
       include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2>STUDENT LEDGER</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> Fees Collection Success.
                            </div>';
						}elseif(@$status == 'editsuccess')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Edited Record.
                            </div>';
						}elseif(@$status == 'deletesuccess')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deleted</strong> You successfully deleted Record.
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                                STUDENT LEDGER
                            </h2>
                            
                        </div>  
						    
						<div class="body">
                            <form method="POST" onsubmit="event.preventDefault();" action="collect-fees-exec.php">
                                 <label for="year">ACADEMIC YAER</label>
								<div class="form-group">
                                            <select class="form-control show-tick" name="yr_id" id="yr_id">
                                                
											<?php 
										include('dbhost.php');
										$query = mysqli_query($con,"SELECT * FROM year WHERE `active_status`='active'");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['y_id'].'">'.$row['year'].'</option>';
                                          
										}
										?>
                                            </select>
                                </div>
								
                                <label for="year">BRANCH</label>
								<div class="form-group">
                                            <select class="form-control show-tick" name="br_id" id="br_id" onchange="getStudent(this.value)">
                                                <option value="">-- Please select --</option>
											<?php 
										include('dbhost.php');
										$query = mysqli_query($con,"SELECT * FROM branch");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['br_id'].'">'.$row['br_name'].'</option>';
                                          
										}
										?>
                                            </select>
                                </div>
								<div id="txt2"></div>
								
								
								<button type="SUBMIT" onclick="getLedger(this.value)"; value="kk" class="btn btn-primary m-t-15 waves-effect">SEARCH</button>
								
                            </form>

                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </section>
     <div id="txt3"></div>
    
  <script language="JavaScript">
  function demo(str)
{
	var a = confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='delete-branch.php?br_id='+str; 
	}
}

function getStudent(str) {
	var y_id = document.getElementById('yr_id').value;
	if (str == "") {
        document.getElementById("txt2").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt2").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getStudents1.php?value="+str+"&y_id="+y_id,true);
        xmlhttp.send();
	
		
    }
    }
	
	function getLedger(str) {
		//var f_date = document.getElementById('f_date').value;
		var yr_id = document.getElementById('yr_id').value;
		var s_id = document.getElementById('s_id').value;
		
	if (str == "") {
        document.getElementById("txt3").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt3").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","getLedger.php?value="+str+"&yr_id="+yr_id+"&s_id="+s_id,true);
        xmlhttp.send();
	
		
    }
    }
  </script>
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
