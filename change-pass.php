<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Change | Pass</title>
    <?php
        include('header_files.php');
		@$status = $_GET['status'];
		@$cr_id = $_GET['cr_id'];
		@$br_id = $_GET['br_id'];
		@$y_id = $_GET['y_id'];
	
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2>Change Password</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong>Password Changed.
                            </div>';
						}elseif(@$status == 'editsuccess')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Edited Record.
                            </div>';
						}elseif(@$status == 'deletesuccess')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deleted</strong> You successfully deleted Record.
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                                Change Password
                            </h2>
                       </div>  
						<div class="body">
                            <form method="POST" action="change-pass-exec.php">
                                <label for="email_address">Enter New Password</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="amt" required name="pass" class="form-control" placeholder="Enter Password">
                                    </div>
                                </div>
								
								 <button type="SUBMIT" class="btn btn-primary m-t-15 waves-effect">SAVE</button>
                            </form>

                        </div>
							</div>
                </div>
            </div>
        </div>
    </section>

    
  <script language="JavaScript">
  function demo(str)
{
	var a = confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='delete-branch.php?br_id='+str; 
	}
}

function getStudent(str) {
	var y_id = document.getElementById('y_id').value;
	if (str == "") {
        document.getElementById("txt2").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt2").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getStudent.php?value="+str+"&y_id="+y_id,true);
        xmlhttp.send();
	
		
    }
    }
	
	function getBal(str) {
	if (str == "") {
        document.getElementById("txt3").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt3").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getBal.php?value="+str,true);
        xmlhttp.send();
	
		
    }
    }
  </script>
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
