<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Fees | Collect</title>
    <?php
        include('header_files.php');
		@$status = $_GET['status'];
		@$cr_id = $_GET['cr_id'];
		@$br_id = $_GET['br_id'];
		@$y_id = $_GET['y_id'];
	
    ?>
</head>
    <?php
        include('header.php');
        include('menu.php');
    ?>
    <section class="content">
        <div class="container-fluid">
							
            <div class="block-header">
                <h2>COLLECT FEES</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> Fees Collection Success.
                            </div>';
						}elseif(@$status == 'editsuccess')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Edited Record.
                            </div>';
						}elseif(@$status == 'deletesuccess')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deleted</strong> You successfully deleted Record.
                            </div>';
						}
						?>
                      <div class="header">
                            <h2>
                                COLLECT FEES
                            </h2>
                            
                        </div>  
						<?php 
if(@$status == 'again')
{					?>
				<div class="body">
                            <form method="POST" action="collect-fees-exec.php">
                                
								<?php 
										include('dbhost.php');
										$query12 = mysqli_query($con,"SELECT * FROM year WHERE `active_status`='active'");
                                        while ($row12 = mysqli_fetch_assoc($query12))
                                        {		
												$y_id = $row12['y_id'];
                                          
										}
										?>
										<input type="hidden"  name="y_id" id="y_id" value="<?php echo $y_id;?>">
								
								
                                <label for="year">BRANCH</label>
								<div class="form-group">
                                            <select class="form-control show-tick" name="br_id" id="br_id" required>
                                               
											<?php 
										include('dbhost.php');
										$query = mysqli_query($con,"SELECT * FROM branch WHERE `br_id`='$br_id'");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['br_id'].'">'.$row['br_name'].'</option>';
                                          
										}
										?>
                                            </select>
                                </div>
								
								<label for="year">COURSE</label>
								<div class="form-group">
                                            <select class="form-control show-tick" name="cr_id" required id="cr_id" onchange="getStudent(this.value)">
                                                
											<?php 
										include('dbhost.php');
										$query = mysqli_query($con,"SELECT * FROM course WHERE `cr_id`='$cr_id'");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['cr_id'].'">'.$row['cr_name'].'</option>';
                                          
										}
										?>
                                            </select>
                                </div>
								
								<div id="txt2">
								<label for="year">STUDENT NAME</label>
                                
                                        <div class="form-group">
                                            <select class="form-control show-tick" required name="s_id" id="s_id" onchange="getBal(this.value)">
                                                <option value="">-- Please select --</option>
											<?php 
										//include('dbhost.php');
										$query = mysqli_query($con,"SELECT * FROM `admissions` WHERE `br_id`='$br_id' AND `y_id`='$y_id' AND `cr_id` LIKE '%$cr_id'");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
											$s_id = $row['s_id'];
											$qry = mysqli_query($con,"SELECT * FROM `student` WHERE `s_id`='$s_id'");
											while($row1 = mysqli_fetch_assoc($qry))
											{
												echo'<option value="'.$row1['s_id'].'">'.$row1['s_name'].'</option>';
											}
										}
										?>
                                            </select>
                                        </div>
								</div>
								<div id="txt3"></div>
								
								
                            </form>

                        </div>
		<?php
}else
{?>
						<div class="body">
                            <form method="POST" action="collect-fees-exec.php">
                                
								<?php 
										include('dbhost.php');
										$query12 = mysqli_query($con,"SELECT * FROM year WHERE `active_status`='active'");
                                        while ($row12 = mysqli_fetch_assoc($query12))
                                        {		
												$y_id = $row12['y_id'];
                                          
										}
										?>
										<input type="hidden"  name="y_id" id="y_id" value="<?php echo $y_id;?>">
								
								
                                <label for="year">BRANCH</label>
								<div class="form-group">
                                            <select class="form-control show-tick" required name="br_id" id="br_id">
                                                <option value="">-- Please select --</option>
											<?php 
										include('dbhost.php');
										$query = mysqli_query($con,"SELECT * FROM branch");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['br_id'].'">'.$row['br_name'].'</option>';
                                          
										}
										?>
                                            </select>
                                </div>
								
								<label for="year">COURSE</label>
								<div class="form-group">
                                            <select class="form-control show-tick" required name="cr_id" id="cr_id" onchange="getStudent(this.value)">
                                                <option value="">--  select Course --</option>
											<?php 
										include('dbhost.php');
										$query = mysqli_query($con,"SELECT * FROM course");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['cr_id'].'">'.$row['cr_name'].'</option>';
                                          
										}
										?>
                                            </select>
                                </div>
								
								<div id="txt2"></div>
								<div id="txt3"></div>
								
								
                            </form>

                        </div>
<?php  }
		?>
						
                    </div>
                </div>
            </div>
        </div>
    </section>

    
  <script language="JavaScript">
  function demo(str)
{
	var a = confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='delete-branch.php?br_id='+str; 
	}
}

function getStudent(str) {
	var y_id = document.getElementById('y_id').value;
	var br_id = document.getElementById('br_id').value;
	if (str == "") {
        document.getElementById("txt2").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt2").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getStudent.php?value="+str+"&y_id="+y_id+"&br_id="+br_id,true);
        xmlhttp.send();
	
		
    }
    }
	
	function getBal(str) {
	if (str == "") {
        document.getElementById("txt3").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt3").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getBal.php?value="+str,true);
        xmlhttp.send();
	
		
    }
    }
  </script>
    <?php
        include('footer_files.php')
    ?>
</body>
</html>
