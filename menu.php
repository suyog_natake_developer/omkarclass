<section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
           
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="dashboard.php">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">person_add</i>
                            <span>Registration</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="add-branch.php">
                                    <i class="material-icons">place</i>
                                    <span>Branch</span>
                                </a>
                            </li>
                            <li>
                                <a href="add-course.php">
                                    <i class="material-icons">person</i>
                                    <span>Course</span>
                                </a>
                            </li>
                            <li>
                                <a href="add-student.php">
                                    <i class="material-icons">contacts</i>
                                    <span>Students</span>
                                </a>
                            </li>
							
							<li>
                                <a href="all-year.php">
                                    <i class="material-icons">date_range</i>
                                    <span>Academic Year</span>
                                </a>
                            </li>
							
							<li>
                                <a href="all-adm.php">
                                    <i class="material-icons">date_range</i>
                                    <span>Deactive Admissions</span>
                                </a>
                            </li>
							
                        </ul>
                    </li>
                   
                   
                       
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">rate_review</i>
                            <span>Fees Collection</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
							    <a href="collect-fees.php">
                                    <i class="material-icons">assignment_returned</i>
                                    <span>Collect Fees</span>
                                </a>
                            </li>
							
							
                            <li>
							    <a href="all-fees.php">
                                    <i class="material-icons">edit</i>
                                    <span>Edit Collect Fees</span>
                                </a>
                            </li>
                        </ul>
						
						<a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">font_download</i>
                            <span>Attendence</span>
                        </a>
                        <ul class="ml-menu">
                           
							<li>
							    <a href="attendence.php">
                                    <i class="material-icons">font_download</i>
                                    <span>Fill Attendence</span>
                                </a>
                            </li>
                           
                        </ul>
						
						<a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">forward</i>
                            <span>Forward Students</span>
                        </a>
                        <ul class="ml-menu">
                           
							<li>
							    <a href="forward.php">
                                    <i class="material-icons">edit</i>
                                    <span>Forward to Next Year</span>
                                </a>
                            </li>
                           
                        </ul>
						
						
						<a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">message</i>
                            <span>Sent Messages</span>
                        </a>
                        <ul class="ml-menu">
                           
							<li>
							    <a href="sent-all.php">
                                    <i class="material-icons">edit</i>
                                    <span>Compose & Sent To All</span>
                                </a>
                            </li>
							
							<li>
							    <a href="fees-due.php">
                                    <i class="material-icons">edit</i>
                                    <span>Fees Due Message</span>
                                </a>
                            </li>
							
							<li>
							    <a href="sent-atten.php">
                                    <i class="material-icons">edit</i>
                                    <span>Sent Attendence</span>
                                </a>
                            </li>
                           
                        </ul>
						
						<a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Reports</span>
                        </a>
                        <ul class="ml-menu">
                           
							<li>
							    <a href="student-ledger.php">
                                    <i class="material-icons">edit</i>
                                    <span>Students Ledger</span>
                                </a>
                            </li>
                           
                        </ul>
						
						<a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Settings</span>
                        </a>
                        <ul class="ml-menu">
                           
							<li>
							    <a href="change-pass.php">
                                    <i class="material-icons">edit</i>
                                    <span>Change Password</span>
                                </a>
								
								<a href="message.php">
                                    <i class="material-icons">edit</i>
                                    <span>On/Off Messages</span>
                                </a>
								
                            </li>
                           
                        </ul>
						
                    </li>
                   
                   
                </ul>
            </div>
            <!-- #Menu -->
           <?php
            include('footer.php');
           ?>

        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" class="active">
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink">
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple">
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                        <li data-theme="indigo">
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue">
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal">
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green">
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime">
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow">
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber">
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange">
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown">
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey">
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                        <li data-theme="blue-grey">
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                        <li data-theme="black">
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="settings">
                    <div class="demo-settings">
                        <p>GENERAL SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Report Panel Usage</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Email Redirect</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>SYSTEM SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Notifications</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Auto Updates</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>ACCOUNT SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Offline</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Location Permission</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>